<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VadminController;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('userlogin');
// })->name('userlogin');


// Route::get('/link-reg', function () {
//     return view('link-reg');
// });

use App\Http\Controllers\FacilityController;

// Route::resource('facilities', FacilityController::class);


Route::middleware('auth', 'verified')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit')->middleware(['auth', 'verified']);
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
});

Route::middleware('auth', 'admin', 'verified')->group(function(){
    Route::get('/dashboard', [VadminController::class, 'Dashboard'])->name('dashboard')->middleware(['auth', 'verified']);
    Route::get('/bookings', [VadminController::class, 'Bookings'])->name('bookings')->middleware(['auth', 'verified']);
    Route::get('/getbookings', [VadminController::class, 'getBookings'])->name('getbookings')->middleware(['auth', 'verified']);
    Route::patch('/bookings/approve', [VadminController::class, 'BookingsApprove'])->name('bookings.approve');
    Route::patch('/bookings/deny', [VadminController::class, 'BookingsDeny'])->name('bookings.deny');


    Route::get('/facilities', [VadminController::class, 'Facilities'])->name('facilities')->middleware(['auth', 'verified']);
    Route::get('/deleteFacility/{id}', [VadminController::class, 'deleteFacility'])->name('deleteFacility')->middleware(['auth', 'verified']);

    Route::post('/facilities/create', [VadminController::class, 'FacilitiesCreate'])->name('facilities.create');
    Route::put('/facilities/update/{id}', [VadminController::class, 'Facilitiesupdate'])->name('facilities.update');


    Route::get('/feedbacks', [VadminController::class, 'Feedbacks'])->name('feedbacks')->middleware(['auth', 'verified']);
    Route::delete('/feedbacks/deletefeedback/{id}',[VadminController::class, 'DeleteFeedback'])->name('user.deleteFeedback');

    Route::get('/user', [VadminController::class, 'User'])->name('user')->middleware(['auth', 'verified']);
    Route::get('/users', [VadminController::class, 'Users'])->name('users')->middleware(['auth', 'verified']);

    Route::delete('/user/deleteuser/{id}',[VadminController::class, 'DeleteUser'])->name('user.deleteUser');

    Route::get('/profile', [VadminController::class, 'Profile'])->name('profile')->middleware(['auth', 'verified']);
    Route::get('/editProfile', [VadminController::class, 'EditProfile'])->name('editProfile')->middleware(['auth', 'verified']);
    Route::post('/editProfile/{id}', [VadminController::class, 'UpdateProfile'])->name('updateProfile');

    Route::post('/changepassword/{id}', [VadminController::class, 'changepassword'])->name('changepassword');
});

Route::middleware('auth', 'user', 'verified')->group(function(){
    Route::get('/user/homepage',[UserController::class, 'Homepage'])->middleware(['auth', 'verified'])->name('user.homepage');
    Route::get('/user/bookingpage/{id}',[UserController::class, 'Bookingpage'])->name('user.bookingpage')->middleware(['auth', 'verified']);
    Route::post('/user/bookingpage/create',[UserController::class, 'BookingpageCreate'])->name('user.bookingpage.create');
    Route::post('/user/feedback',[UserController::class, 'FeedbackPost'])->name('feedback.store');

    Route::get('/user/feedback',[UserController::class, 'Feedback'])->name('user.feedback')->middleware(['auth', 'verified']);
    Route::get('/user/mybooking',[UserController::class, 'Mybooking'])->name('user.mybooking')->middleware(['auth', 'verified']);
    Route::delete('/user/deletebooking/{id}',[UserController::class, 'DeleteBooking'])->name('user.deleteBooking');

    Route::get('/user/profile',[UserController::class, 'Profile'])->name('user.profile')->middleware(['auth', 'verified']);
    Route::put('/user/{user}', [UserController::class, 'update'])->name('user.update');
    Route::delete('/user/{user}', [UserController::class, 'destroy'])->name('users.destroy');


    Route::get('/user/mybooking2',[UserController::class, 'mybooking2'])->name('mybookings')->middleware(['auth', 'verified']);
});


require __DIR__.'/auth.php';
