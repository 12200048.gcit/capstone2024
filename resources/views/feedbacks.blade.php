<x-app-layout>
    <style>
        .btn-outline-primary {
        padding: 5px 12px; /* Adjust padding to make it smaller */
        font-size: 12px; /* Decrease font size */
        margin-top: 15px;
    }

        .btn-outline-primary:hover {
            color: #fff; /* Change text color to white */
            background-color: #dc3545; /* Change background color to red */
            border-color: #dc3545; /* Change border color to red */  
        }
        
    </style>
    <link rel="stylesheet" href="{{asset('../../../assets/css/feedback.css')}}">
    <h1>Feedbacks</h1>
    <div class="maincontainer">
        <!-- <div class="row">
            <p>User Feedbacks</p>
        </div> -->
        <div class=" table-responsive">
            <table class="table-info w-100">
                <thead>
                    <tr>
                        <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                        <th scope="col" style="color:white; padding-Left:5px">Feedbacks</th>
                        <th scope="col" style="color:white; padding-Left:5px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                      $sl = 1;  
                    @endphp
                    @foreach ($feedbacks as $feedback)
                    <tr>
                        <th scope="row">{{$sl}}</th>
                          <td style="font-size:12px; padding-Left:15px">{{$feedback->content}}</td>
                        <td>
                            {{-- <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                Delete
                            </button> --}}

                            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                Delete
                            </button>

                            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="staticBackdropLabel">Delete feedback</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to delete this Feedback?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                            <form action="{{ route('user.deleteFeedback', $feedback->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button style="color:rgb(218, 64, 64)" type="submit" class="btn btn-primary">Yes</button>
                                            </form>                                        
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </td>
                    </tr>
                    @php
        $sl++; // Increment serial number for each iteration
    @endphp
                    @endforeach

                </tbody>
            </table>
            <p style="text-align:center">
                << @if ($feedbacks->lastPage() > 1)
                    @for ($i = 1; $i <= $feedbacks->lastPage(); $i++)
                        <a style="margin-inline: 10px" href="{{ $feedbacks->url($i) }}">{{ $i }}</a>
                    @endfor
                @endif >>
            </p>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</x-app-layout>