<x-app-layout>
    <link rel="stylesheet" href="{{asset('../../../assets/css/booking.css')}}">
    <h1>Bookings</h1>
    <div class="maincontainer">
        <div class="row">
            <div class="d-flex">
                <div class="p-2 ms-auto">
                    <div class="dropdown">
                        
                        <form id="userTypeForm" action="{{route('bookings')}}" method="GET">
                            @csrf
                            <select style="height: 31px; border-radius: 5px"  id="userType" name="userType">
                                <option>Select Role</option>
                                <option value="student">Student</option>
                                <option value="staff">Staff</option>
                            </select>
                        </form>
                        
                    </div>
                </div>
                
                <div class=" p-2">
                    <div class="input-group input-group-sm mb-3">
                        <input id="searchInput" onkeyup="filterTable()" type="text" class="form-control border-dark" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Search by ID">
                        <span class="input-group-text border-dark" id="inputGroup-sizing-sm"><i class="fas fa-search"></i></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive p-3">        
            <table class="table-info w-100" id="bookingsTable">
                <thead>
                    <tr>
                        <th scope="col" style="color:white; padding-left:5px">Sl.no</th>
                        <th id="headerLabel" scope="col" style="color:white; padding-left:5px">ID</th>
                        <th scope="col" style="color:white; padding-left:5px">Facility</th>
                        <th scope="col" style="color:white; padding-left:5px">Date</th>
                        <th scope="col" style="color:white; padding-left:5px">Time</th>
                        <th scope="col" style="color:white; padding-left:5px">Status</th>
                        <th scope="col" style="color:white; padding-left:5px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $sl = 1; @endphp
                    @foreach ($bookings as $booking)
                    <tr>
                        <th scope="row">{{$sl}}</th>
                        <td style="font-size:12px; padding-left:5px">{{$booking->users->employeeid}}</td>
                        <td style="font-size:12px; padding-left:5px">{{$booking->facility->facility_name}}</td>
                        <td style="font-size:12px; padding-left:5px">{{$booking->date}}</td>
                        <td style="font-size:12px; padding-left:5px">{{$booking->starttime}}-{{$booking->endtime}}</td>
                        @if($booking->status === 'Pending')
                            <td style="font-size:12px;padding-Left:5px;color:blue">{{$booking->status}}</td>
                        @elseif($booking->status === 'Accepted')
                            <td style="font-size:12px;padding-Left:5px;color:green">{{$booking->status}}</td>
                        @else
                            <td style="font-size:12px;padding-Left:5px;color:red">{{$booking->status}}</td>
                        @endif
                        <td>
                            <button style="font-size: 12px; padding: 2px 8px; height: 30px;" class="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal{{$booking->id}}" data-bs-whatever="@getbootstrap" >View</button>

                            <div class="modal fade" id="exampleModal{{$booking->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Booking Details</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Username</label>
                                                    <p>{{$booking->users->name}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">E-mail Id</label>
                                                    <p>{{$booking->users->email}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Student Id/employee Id</label>
                                                    <p>{{$booking->users->employeeid}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Date</label>
                                                    <p>{{$booking->date}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Time</label>
                                                    <p>{{$booking->starttime}}-{{$booking->endtime}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label">Facility Name</label>
                                                    <p>{{$booking->facility->facility_name}}</p>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="message-text" class="col-form-label">Reason for booking</label>
                                                    <p>{{$booking->reason}}</p>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <form action="{{route('bookings.deny')}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method('patch')
                                                <input type="hidden" value="{{$booking->id}}" name="booking_id">
                                                <button type="submit" class="btn btn-primary">Deny</button>
                                            </form>
                                            <form action="{{route('bookings.approve')}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method('patch')
                                                <input type="hidden" value="{{$booking->id}}" name="booking_id">
                                                <button type="submit" class="btn btn-primary">Approve</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        @php $sl++; @endphp
                    @endforeach
                    </tr>
                </tbody>
            </table>
            <p style="text-align:center">
                << @if ($bookings->lastPage() > 1)
                    @for ($i = 1; $i <= $bookings->lastPage(); $i++)
                        <a style="margin-inline: 10px" href="{{ $bookings->url($i) }}">{{ $i }}</a>
                    @endfor
                @endif >>
            </p>
        </div>
        {{-- search booking data by student id --}}
        <script>
            function filterTable() {
                let input = document.getElementById('searchInput');
                let filter = input.value.toUpperCase();
                let table = document.getElementById('bookingsTable');
                let tr = table.getElementsByTagName('tr');

                for (let i = 1; i < tr.length; i++) { // Start from 1 to skip the header row
                    let td = tr[i].getElementsByTagName('td')[0]; // Assume Student ID is in the first column
                    if (td) {
                        let txtValue = td.textContent || td.innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }
        </script>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script>
        
        $(document).ready(function() {
            $('#userType').on('change', function() {
                $('#userTypeForm').submit();
            });
        });

    </script>

</x-app-layout>