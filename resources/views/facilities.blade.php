<x-app-layout>

    <style>
        .btn-outline-primary1 {
        border: 1px solid blue;
        color: white;
        background-color: transparent;
        border-radius: 0.25rem;
        padding: 0.25rem 0.5rem;
        font-size: 0.875rem;  
        
    }

        .btn-outline-primary1:hover {
            color: white; /* Change text color to white */
            background-color: #dc3545; /* Change background color to red */
            border-color: #dc3545; /* Change border color to red */  
        }
        
    </style>

    <link rel="stylesheet" href="{{asset('../../../assets/css/facility.css')}}">
    <h1>Facilities</h1>
    <!-- Display Success Message -->
    @if(session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <!-- Display Error Messages -->
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="maincontainer">
        <div class="row">
            <div class="col-lg-8 p-3">
                <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">
                    Add
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Add Facility</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('facilities.create')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="recipient-name" class="col-form-label text-secondary">Facility Name</label>
                                        <input name="facility_name" type="text" class="form-control border-dark" 
                                        id="facility-name"
                                        required>
                                       
                                        
                                    </div>
                                    <div class="mb-3">
                                        <label for="message-text" class="col-form-label text-secondary">Description</label>
                                        <textarea name="desc" class="form-control border-dark" id="message-text"></textarea>
                                    </div>
                                    <div>
                                        <label for="recipient-name" class="col-form-label text-secondary">Time</label>
                                    </div>

                                    <div class="mb-3 form-check-inline" style="display:flex;flex-direction:column">
                                        <div class="input-group">
                                            <input class="form-control" name="starttime" type="time" value="00:00">
                                            <input class="form-control" name="endtime" type="time" value="00:00">
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="message-text" class="col-form-label text-secondary">Time Slot</label>
                                        <fieldset class="input-group">
                                            <input class="form-control" type="number" name="hour" value="0" aria-label="hour" aria-describedby="hour-description" min="0" max="24" placeholder="hour" />
                                            <span class="input-group-text">:</span>
                                            <input class="form-control" type="number" name="minute" value="0" aria-label="minute" min="0" max="59" placeholder="minute" />
                                        </fieldset>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input name="role" class="form-check-input border-dark" type="radio" value="all" id="flexCheckDefault">
                                        <label class="form-check-label text-secondary" for="flexCheckDefault">
                                            All
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input name="role" class="form-check-input border-dark" type="radio" value="staff" id="flexCheckChecked">
                                        <label class="form-check-label text-secondary" for="flexCheckChecked">
                                            Only Staff
                                        </label>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input name="image" accept="image/jpeg, image/png" type="file" class="form-control border-dark" id="inputGroupFile01">
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input border-dark" type="radio" name="status" value="available" id="flexRadioDefault1">
                                        <label class="form-check-label text-success" for="flexRadioDefault1">
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input border-dark" type="radio" name="status" value="unavailable" id="flexRadioDefault2">
                                        <label class="form-check-label text-danger" for="flexRadioDefault2">
                                            Unavailable
                                        </label>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Add</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
               
            </div>
            <div class="col-lg-4 p-3">
                <div class="input-group input-group mb-3">
                    <input id="searchInput" type="text" class="form-control border-dark" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Search by facility name">
                    <span class="input-group-text border-dark" id="inputGroup-sizing-sm"><i class="fas fa-search"></i></span>
                </div>
                <!-- <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div> -->
            </div>
        </div>

        <div class=" table-responsive p-3">
            <table id="facilityTable" class="table-info w-100">
                <thead>
                    <tr>
                        <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                        <th scope="col" style="color:white; padding-Left:5px">Image</th>
                        <th scope="col" style="color:white; padding-Left:5px">Facility</th>
                        <th scope="col" style="color:white; padding-Left:5px">Descriptions</th>
                        <th scope="col" style="color:white; padding-Left:5px">status</th>
                        <th scope="col" style="color:white; padding-Left:25px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                      $sl = 1;  
                    @endphp
                    @foreach ($facilities as $facility)
                        
                    
                    <tr>
                        <th scope="row">{{$sl}}</th>
                        <td><img src="{{ Storage::url($facility->image) }}" width="70" height="50"></td>
                        <td style="font-size:12px; padding-Left:5px">{{$facility->facility_name}}</td>
                        <td style="font-size:12px;padding-Left:5px">{{$facility->desc}}</td>
                        {{-- <td style="color:#27A806; font-size:12px; padding-Left:5px">{{$facility->status}}</td>
                         --}}
                         @if ($facility->status === 'Available')
                                        <td style="color:#27A806; font-size:12px; padding-Left:5px">Avaliable</td>
                                    @else
                                        <td style="color:red; font-size:12px; padding-Left:5px">Unavailable</td>
                                    @endif

                        <td>
                            <div style="display: flex;flex-direction:row">

                            <button type="button" class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal{{ $facility->id }}" data-bs-whatever="@getbootstrap" style="padding: 0.25rem 0.5rem; margin-right: 10px;"
                             data-bs-whatever="@getbootstrap"
                            >
                                Edit
                            </button>


                            <button type="button" class="btn btn-outline-primary1" data-bs-whatever="@getbootstrap" style="font-size: 0.875rem; padding: 0.25rem 0.5rem;">
                                   <a href="{{route('deleteFacility',['id' => $facility->id])}}" style="text-decoration: none; transition: color 0.3s;">Delete</a>
                            </button>
                            </div>
                            
                            <div class="modal fade" id="exampleModal{{ $facility->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Facility</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('facilities.update', $facility->id)}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method('put')
                                                <div class="mb-3">
                                                    <label for="recipient-name" class="col-form-label text-secondary">Facility Name</label>
                                                    <input name="facility_name" type="text" class="form-control border-dark" 
                                                    id="facility-name" value="{{$facility->facility_name}}"
                                                    required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="message-text" class="col-form-label text-secondary">Description</label>
                                                    <textarea name="desc" class="form-control border-dark" id="message-text" value="{{$facility->desc}}">{{$facility->desc}}</textarea>
                                                </div>
                                                <div>
                                                    <label for="recipient-name" class="col-form-label text-secondary">Time</label>
                                                </div>
            
                                                <div class="mb-3 form-check-inline" style="display:flex;flex-direction:column">
                                                    <div class="input-group">
                                                        <input class="form-control" name="starttime" type="time" value="{{\Carbon\Carbon::parse($facility->starttime)->format('H:i')}}">
                                                        <input class="form-control" name="endtime" type="time" value="{{\Carbon\Carbon::parse($facility->endtime)->format('H:i')}}">
                                                    </div>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="message-text" class="col-form-label text-secondary">Time Slot</label>
                                                    <fieldset class="input-group">
                                                        <input class="form-control" type="number" name="hour" value="0" aria-label="hour" aria-describedby="hour-description" min="0" max="24" placeholder="hour" />
                                                        <span class="input-group-text">:</span>
                                                        <input class="form-control" type="number" name="minute" value="0" aria-label="minute" min="0" max="59" placeholder="minute" />
                                                    </fieldset>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input name="role" class="form-check-input border-dark" type="radio" value="all" id="flexCheckDefault" {{$facility->role === 'all' ? 'checked' : ''}}>
                                                    <label class="form-check-label text-secondary" for="flexCheckDefault">
                                                        All
                                                    </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input name="role" class="form-check-input border-dark" type="radio" value="staff" id="flexCheckChecked" {{$facility->role === 'staff' ? 'checked' : ''}}>
                                                    <label class="form-check-label text-secondary" for="flexCheckChecked">
                                                        Only Staff
                                                    </label>
                                                </div>
                                                <div class="input-group mb-3">
                                                    <input name="image" accept="image/jpeg, image/png" value="{{$facility->image}}" type="file" class="form-control border-dark" id="inputGroupFile01">
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input border-dark" type="radio" name="status" value="available" id="flexRadioDefault1" {{$facility->status === 'available' ? 'checked' : ''}}>
                                                    <label class="form-check-label text-success" for="flexRadioDefault1">
                                                        Available
                                                    </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input border-dark" type="radio" name="status" value="unavailable" id="flexRadioDefault2" {{$facility->status === 'unavailable' ? 'checked' : ''}}>
                                                    <label class="form-check-label text-danger" for="flexRadioDefault2">
                                                        Unavailable
                                                    </label>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @php
                    $sl++
                    @endphp
                    @endforeach
                </tbody>
            </table>
            <p style="text-align:center">
                << @if ($facilities->lastPage() > 1)
                    @for ($i = 1; $i <= $facilities->lastPage(); $i++)
                        <a style="margin-inline: 10px" href="{{ $facilities->url($i) }}">{{ $i }}</a>
                    @endfor
                @endif >>
            </p>
            <script>
                // Get the input field and table
                var input = document.getElementById("searchInput");
                var table = document.getElementById("facilityTable");
                var rows = table.getElementsByTagName("tr");
            
                // Add event listener to the input field
                input.addEventListener("input", function() {
                    var filter = input.value.toUpperCase();
            
                    // Loop through all table rows, and hide those who don't match the search query
                    for (var i = 0; i < rows.length; i++) {
                        var facilityNameColumn = rows[i].getElementsByTagName("td")[1]; // Adjust index if needed
                        if (facilityNameColumn) {
                            var textValue = facilityNameColumn.textContent || facilityNameColumn.innerText;
                            if (textValue.toUpperCase().indexOf(filter) > -1) {
                                rows[i].style.display = "";
                            } else {
                                rows[i].style.display = "none";
                            }
                        }
                    }
                });
            </script>
            {{-- <div style="display: flex; justify-content: center; margin-Top:30px; margin-Bottom:-15px">
                <nav aria-label="Page navigation example">
                    <ul class="pagination ">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div> --}}

        </div>
        <!-- <script src="https://cdn.jsdelivr.net/npm/chart.js"></script> -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
        {{-- <script>
            document.addEventListener('DOMContentLoaded', (event) => {
                const exampleModal = document.getElementById('exampleModal');
                exampleModal.addEventListener('show.bs.modal', function (event) {
                    const button = event.relatedTarget;
                    const facilityId = button.getAttribute('data-id');
                    const facilityName = button.getAttribute('data-name');
                    const facilityDesc = button.getAttribute('data-desc');
                    const facilityStatus = button.getAttribute('data-status');
                    const facilityStarttime = button.getAttribute('data-starttime');
                    const facilityEndtime = button.getAttribute('data-endtime');
                    const facilityHour = button.getAttribute('data-hour');
                    const facilityMinute = button.getAttribute('data-minute');
                    const facilityRole = button.getAttribute('data-role').split(',');
        
                    const modal = this;
                    modal.querySelector('#facility-id').value = facilityId;
                    modal.querySelector('#facility-name').value = facilityName;
                    modal.querySelector('#facility-desc').value = facilityDesc;
                    modal.querySelector('#facility-starttime').value = facilityStarttime;
                    modal.querySelector('#facility-endtime').value = facilityEndtime;
                    modal.querySelector('#facility-hour').value = facilityHour;
                    modal.querySelector('#facility-minute').value = facilityMinute;
        
                    facilityRole.forEach(role => {
                        if (role === 'all') {
                            modal.querySelector('#facility-role-all').checked = true;
                        } else if (role === 'staff') {
                            modal.querySelector('#facility-role-staff').checked = true;
                        }
                    });
        
                    if (facilityStatus === 'available') {
                        modal.querySelector('#facility-status-available').checked = true;
                    } else if (facilityStatus === 'unavailable') {
                        modal.querySelector('#facility-status-unavailable').checked = true;
                    }
                });
            });
        </script> --}}

        <script>
            document.addEventListener('DOMContentLoaded', function () {
                var exampleModal = document.getElementById('exampleModal');
                exampleModal.addEventListener('show.bs.modal', function (event) {
                    var button = event.relatedTarget;
                    var facilityId = button.getAttribute('data-id');
                    var facilityName = button.getAttribute('data-name');
                    var facilityDesc = button.getAttribute('data-desc');
                    var facilityStatus = button.getAttribute('data-status');
        
                    var modalTitle = exampleModal.querySelector('.modal-title');
                    var inputName = exampleModal.querySelector('#recipient-name');
                    var inputDesc = exampleModal.querySelector('#message-text');
                    var inputStatus = exampleModal.querySelector('input[name="status"][value="' + facilityStatus + '"]');
        
                    modalTitle.textContent = 'Edit Facility';
                    inputName.value = facilityName;
                    inputDesc.value = facilityDesc;
                    inputStatus.checked = true;
        
                    exampleModal.querySelector('form').action = '/facilities/' + facilityId;
                });
            });
        </script>
        
        
        
</x-app-layout>