<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="National Land Commission Secretary Scheduling Appointment System (NLCSSAS)">
    <meta name="keywords" content="NLCSSAS, NLCS, NLCS Appointment">

    <title>{{ config('app.name', 'Sherubtse College') }}</title>

    <!-- Favicon  -->
    <link rel="icon" href="{{ asset('lg.png') }}" />

    {{-- font --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">

    {{-- Bootstrap style --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css" integrity="sha512-6qkvBbDyl5TDJtNJiC8foyEVuB6gxMBkrKy67XpqnIDxyvLLPJzmTjAj1dRJfNdmXWqD10VbJoeN4pOQqDwvRA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.11.3/font/bootstrap-icons.min.css" integrity="sha512-dPXYcDub/aeb08c63jRq/k6GaKccl256JQy/AnOq7CAnEZ9FzSL9wSbcZkMp4R26vBsMLFYH4kQ67/bbV8XaCQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>

    {{-- alert --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">

    {{-- style --}}
    <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">

    <style>
        .avatar {
            width: 30px; /* Adjust the size as needed */
            height: 30px; /* Adjust the size as needed */
            background-color: white; /* Adjust the color as needed */
            color: #2764AC;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 20px; /* Adjust the font size as needed */
            text-transform: uppercase;
            font-weight: bold; 
            text-decoration: none;
        }
    </style>

</head>

<body>
    <!-- topnav  -->

    <nav class="navbar sticky-top" style="background-color: #2764AC;">
        <div class="container-fluid">



            <div class="ms-3 me-3 ms-auto">
                <a href="{{route('profile')}}" class="d-flex align-items-center">
                    {{-- <img class="rounded-circle" src="{{ asset('user.PNG') }}" width="35" height="35"> --}}
                    <div class="avatar">
                        {{ substr(Auth::user()->name, 0, 1) }}
                    </div>
                    <p class="mt-3 ms-2 text-white fw-bold" style="text-transform: uppercase; text-decoration: none;" >{{ Auth::user()->name }}</p>
                </a>
            </div>

            <a class="nav-link sidebar-toggler flex-shrink-0 ms-3 d-lg-none d-block "><i class="bi bi-list text-white"></i></a>

        </div>
    </nav>

    <!-- code for side bar -->

    <div class="d-flex">

        <div id="sidebar" class="pt-3 text-white" style="background: linear-gradient(180deg, #2764AC, #2764AC, #443F77);">
            <a class="navbar-brand ms-5" href="{{ route('dashboard') }}"><img src="{{ asset('lg.png') }}" width="100px"></a>
            <div class="mt-4 ms-4">

                <div class="ms-1">
                    <ul class="navbar-nav mb-2 mb-lg-0">
                        <li class="nav-item pe-1 mb-1">
                            <a class="nav-link ps-2 {{ request()->routeIs('dashboard') ? 'active' : '' }}" href="{{ route('dashboard') }}"><i class='bi bi-ui-checks-grid me-2 fs-5'></i>Dashboard</a>
                        </li>
                        <li class="nav-item pe-1">
                            <a class="nav-link ps-2 mb-1 {{ request()->routeIs('facilities') ? 'active' : '' }}" href="{{ route('facilities') }}" ><i class='bi bi-houses me-2 fs-5'></i>Facilities</a>
                        </li>

                        <li class="nav-item pe-1">
                            <a class="nav-link mb-1 ps-2 {{ request()->routeIs('bookings') ? 'active' : '' }}" href="{{ route('getbookings') }}" ><i     class="bi bi-list-check me-2 fs-5"></i>Bookings</a>
                        </li>
                        <li class="nav-item pe-1">
                            <a class="nav-link ps-2 mb-1 {{ request()->routeIs('feedbacks') ? 'active' : '' }}" href="{{ route('feedbacks') }}" ><i class='bi bi-journal-text me-2 fs-5'></i></i>Feedbacks</a>


                        </li>
                        <li class="nav-item pe-1">
                            <a class="nav-link ps-2 mb-1 {{ request()->routeIs('user') ? 'active' : '' }}" href="{{route('user')}}"><i class='bi bi-people me-2 fs-5'></i></i>Users</a>
                        </li>
                        <li class="nav-item pb-5 pe-1">
                            <form method="POST" action="{{ route('logout') }}" x-data>
                                @csrf
                                <button type="submit" class="nav-link ps-2 w-100 text-start" href="{{ route('logout') }}" @click.prevent="$root.submit();"><i class="bi bi-box-arrow-left fs-5 me-2"></i>Logout</button>
                            </form>
                        </li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="content p-3">
            <div class="p-3">
                {{ $slot }}
            </div>

        </div>
    </div>





    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js" integrity="sha512-Yk47FuYNtuINE1w+t/KT4BQ7JaycTCcrvlSvdK/jry6Kcxqg5vN7/svVWCxZykVzzJHaxXk5T9jnFemZHSYgnw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.7/dist/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="{{ asset('dist/js/alertify.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#sidebar").mCustomScrollbar({
                theme: "my-theme"
            });
        });
    </script>

    <script>
        function toggleFullscreen() {
            if (!document.fullscreenElement && // alternative standard method
                !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement
            ) { // current working methods
                var elem = document.documentElement;
                if (elem.requestFullscreen) {
                    elem.requestFullscreen();
                } else if (elem.webkitRequestFullscreen) {
                    /* Safari */
                    elem.webkitRequestFullscreen();
                } else if (elem.msRequestFullscreen) {
                    /* IE11 */
                    elem.msRequestFullscreen();
                }
            } else {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.webkitExitFullscreen) {
                    /* Safari */
                    document.webkitExitFullscreen();
                } else if (document.msExitFullscreen) {
                    /* IE11 */
                    document.msExitFullscreen();
                }
            }
        }
    </script>

    <script>
        $('.sidebar-toggler').click(function() {
            $('#sidebar, .content').toggleClass("open");
            return false;
        });
    </script>
</body>

</html>