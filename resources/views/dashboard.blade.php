<x-app-layout>
    <link rel="stylesheet" href="{{asset('../../../assets/css/dashboard.css')}}">
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1 class="he">DashBoard</h1>
            </div>
        </div>

        <div class="row g-4 mb-4">
            <div class="col-lg-8" style="min-height: 600px;">
                <div class="maincontainer table-responsive p-3">
                    <table class="table-info w-100">
                        <thead>
                            <tr>
                                <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                                <th scope="col" style="color:white; padding-Left:5px">Image</th>
                                <th scope="col" style="color:white; padding-Left:5px">Facility</th>
                                <th scope="col" style="color:white; padding-Left:5px">status</th>
                                <th scope="col" style="color:white; padding-Left:5px">Descriptions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $serialNumber = 1; // Initialize the serial number counter
                            @endphp
                            @foreach ($facilities as $facility)
                                <tr>
                                    <th scope="row">{{$serialNumber}}</th>
                                    <td><img src="{{ asset('storage/' . $facility->image) }}" width="70" height="50"></td>
                                    <td style="font-size:12px; padding-Left:5px">{{$facility->facility_name}}</td>
                                    @if ($facility->status === 'Available')
                                        <td style="color:#27A806; font-size:12px; padding-Left:5px">Avaliable</td>
                                    @else
                                        <td style="color:red; font-size:12px; padding-Left:5px">Unavailable</td>
                                    @endif
                                    <td style="font-size:12px;padding-Left:5px">{{$facility->desc}}</td>
                                </tr>
                                @php
                                    $serialNumber++; // Increment the serial number for the next iteration
                                @endphp
                            @endforeach
                            
                        </tbody>
                    </table>
                    <p style="text-align:center">
                        << @if ($facilities->lastPage() > 1)
                            @for ($i = 1; $i <= $facilities->lastPage(); $i++)
                                <a style="margin-inline: 10px" href="{{ $facilities->url($i) }}">{{ $i }}</a>
                            @endfor
                        @endif >>
                    </p>
                    
                </div>
                <div class="maincontainer table-responsive mt-4 p-3">

                    <table class="table-info w-100 h-100" >
                        <thead>
                            <tr >
                                <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                                <th scope="col" style="color:white; padding-Left:5px">Employ ID</th>
                                <th scope="col" style="color:white; padding-Left:5px">Facility</th>
                                <th scope="col" style="color:white; padding-Left:5px">Date</th>
                                <th scope="col" style="color:white; padding-Left:5px">Time</th>
                                <th scope="col" style="color:white; padding-Left:5px">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $sl = 1; // Initialize the serial number counter
                            @endphp
                            @foreach ($bookings as $booking)
                                <tr>
                                    <th scope="row">{{$sl}}</th>
                                    <td style="font-size:12px; padding-Left:5px">{{$booking->users->employeeid}}</td>
                                    <td style="font-size:12px; padding-Left:5px">{{$booking->facility->facility_name}}</td>
                                    <td style="font-size:12px; padding-Left:5px">{{$booking->date}}</td>
                                    <td style="font-size:12px;padding-Left:5px">{{$booking->starttime}}-{{$booking->endtime}}</td>
                                    {{-- <td style="font-size:12px;padding-Left:5px">{{$booking->status}}</td> --}}
                                    @if($booking->status === 'Pending')
                                        <td style="font-size:12px;padding-Left:5px;color:blue">{{$booking->status}}</td>
                                    @elseif($booking->status === 'Accepted')
                                        <td style="font-size:12px;padding-Left:5px;color:green">{{$booking->status}}</td>
                                    @else
                                        <td style="font-size:12px;padding-Left:5px;color:red">{{$booking->status}}</td>
                                    @endif
                                </tr>
                                @php
                                    $sl++; // Increment the serial number for the next iteration
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <p style="text-align:center">
                        << @if ($bookings->lastPage() > 1)
                            @for ($i = 1; $i <= $bookings->lastPage(); $i++)
                                <a style="margin-inline: 10px" href="{{ $bookings->url($i) }}">{{ $i }}</a>
                            @endfor
                        @endif >>
                    </p>
                </div>

            </div>
            <div class="col-lg-4 d-flex flex-column gap-4" style="min-height: 600px;">
                <div class="states h-25 p-3 text-white">
                    <h6 class="">Sherubtse College<br>
                        <a class="text-white" style="font-size: 10px;">https://www.sherubtse.edu.bt/</a>
                    </h6>
                    <div class="d-flex mt-4">
                        <h6>{{$rowCount}} <br>Facilities</h6>
                        <h6 class="ms-3">{{$countBook}} <br>Booking</h6>
                    </div>
                    <div class="text-center" style="margin-left:60%; margin-top:-80px">
                        <h6>{{$totalUsers}}</h6>
                        <h6>Users</h6>
                    </div>

                </div>
                <div class="h-75 maincontainer">
                    <div class="pt-3 px-3 mb-4">
                        <input class="form-control" type="number" min="2020" max="2024" step="1" value="2024" />
                    </div>
                    <div class="text-center my-2 mt-5">
                        <h5>Facility Usage Pie Chart</h5>
                    </div>
                    <div id="chart"></div>
                </div>
            </div>
        </div>
    </div>

    @php
        // Fetch the count based on facility_id
        $bookingCounts = \App\Models\Booking::select('facility_id', DB::raw('count(*) as total'))
            ->groupBy('facility_id')
            ->get();

        // Fetch facility names using facility_id
        $facilities = [];
        foreach ($bookingCounts as $bookingCount) {
            $facility = \App\Models\Facility::find($bookingCount->facility_id);
            if ($facility) {
                $facilities[] = [
                    'facility_name' => $facility->facility_name,
                    'total' => $bookingCount->total
                ];
            }
        }

        // Prepare data for the chart
        $chartData = [
            'series' => collect($facilities)->pluck('total')->toArray(),
            'labels' => collect($facilities)->pluck('facility_name')->toArray()
        ];

    @endphp



    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    

    <script>

const chartData = @json($chartData);

        const chartOptions = {
            chart: {
                type: 'pie'
            },
            series: chartData.series,
            labels: chartData.labels,
            // Legend configuration
            legend: {
                position: 'top',
                horizontalAlign: 'left',
                formatter: function(seriesName) {
                    return seriesName + "<br>";
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        };

        // Initialize the chart
        const chart = new ApexCharts(document.querySelector("#chart"), chartOptions);

        // Render the chart
        chart.render();
    </script>

   

</x-app-layout>