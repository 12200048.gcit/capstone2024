<x-app-layout>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <section style="padding-block: 30px;display:flex;justify-content:space-evenly">
        <div class="card border-light mb-3" style="max-width: 18rem;height:25rem">
          <div class="card-header" style="font-weight:600;font-size:18px">User Details</div>
          <div class="card-body">
            <span style="font-weight:600">Username</span>
            <p class="card-text">{{ Auth::user()->name }}</p>
            <span style="font-weight:600">Email ID</span>
            <p class="card-text">{{ Auth::user()->email }}</p>
            <span style="font-weight:600">Employee ID</span>
            <p class="card-text">{{ Auth::user()->employeeid }}</p>
            <span style="font-weight:600">Contact No.</span>
            <p class="card-text">{{ Auth::user()->contact }}</p>
            <button type="button" class="btn btn-primary" style="width:100%;margin-bottom:20px"><a href="{{route('editProfile')}}" style="text-decoration: none;color:white">Edit Profile</a></button>

          </div>
        </div>
        <div class="card border-light mb-3" style="width: 20rem;height:22rem;">
          <div class="card-header" style="font-weight:600;font-size:18px; ">Change Password </div>

            <form action="{{route('changepassword',['id'=>Auth::user()->id ])}}" style="padding: 1rem; display:flex; flex-direction:column; align-item:center; justify-contet:center" method="POST">
                @csrf
                <span style="font-weight:600">Old password</span>
                <input type="password" id="name" name="oldpassword" placeholder="Old password" ><br>
                <span style="font-weight:600">New password</span>
                <input type="password" id="name" name="newpassword" placeholder="New password"><br>
                <span style="font-weight:600">Confirm new password</span>
                <input type="password" id="name" name="confirmpassword" placeholder="Confirm new password"><br>
                <button type="submit" class="btn btn-primary" style="width:100%;margin-bottom:20px" >Change Password</button>
            </form>

          </div>
        </div>
        
      </section>

      @if(Session::has('success'))
      <script>
          swal("Message", "{{ Session::get('success') }}", 'success', {
              button: "OK",
              timer: 3000,
          })
      </script>
      @endif
    @if(Session::has('error'))
            <script>
                swal("Message", "{{ Session::get('error') }}", 'error', {
                    button: "OK",
                    timer: 3000,
                })
            </script>
      @endif
</x-app-layout>