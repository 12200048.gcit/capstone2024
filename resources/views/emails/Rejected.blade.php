<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Alloc8</title>
</head>

<body class="ms-5">
  
    <div class="c-email">  
  <div class="c-email__header">
    <h1 class="c-email__header__title" style="font-size:16px">Your recent booking of {{$facility}} has been DENIED</h1>
  </div>
  <div class="c-email__content">
    <div class="c-email__code text-center" style="align-items:center">
      <span class="c-email__code__text" style="padding:10px;background-color:red;color:white;border-radius:5px">DENIED</span>
    </div>
  </div>
  <div class="c-email__footer"></div>
</div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</body>

</html>
<style >

* { box-sizing: border-box; }
body {
  background-color: #fafafa;
  display: flex;
  justify-content: center;
  align-items: center;
}

  &__content {
    width: 100%;
    height: 300px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    flex-wrap: wrap;
    background-color: #fff;
    padding: 15px;
    &__text {
      font-size: 20px;
      text-align: center;
      color: #343434;
      margin-top: 0;
    }
  }
  &__code {
    display: block;
    width: 60%;
    margin: 30px auto;
    background-color: #ddd;
    border-radius: 40px;
    padding: 20px;
    text-align: center;
    font-size: 36px;
    letter-spacing: 10px;
    box-shadow: 0px 7px 22px 0px rgba(0, 0, 0, .1);
  }
  &__footer {
    width: 100%;
    height: 60px;
    background-color: #fff;
  }

.text-center {
  text-align: center;
}
.text-italic {
  font-style: italic;
}
.opacity-30 {
  opacity: 0.3;
}
.mb-0 {
  margin-bottom: 0;
}
</style>