<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/registration.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" />
    <title>Document</title>
</head>
<body>

    <div class="card1">
         <img class='bg' src="{{asset('img/bg.jpeg')}}"/>
         <div >
            <p class="text">Welcome Back</p>
            <p class="text1">
                Gain access to our facility booking system to easily <br> reserve spaces and amenities for your events <br> and activities. Log in now to manage your<br> bookings and streamline your <br>planning process.
            </p>
            <p class="text3">Already have an account?</p>
         
            <a class='button' href="{{route('login')}}">Login Now</a>

         </div>   
    </div>
    

    <div class="card">
    <img class="rlogo" src="{{asset('img/logo.png')}}" />
    <form action="{{route('register')}}" method="POST">
        @csrf
        <input type="text" name="name" placeholder="Name" required>
        <x-input-error :messages="$errors->get('name')"/>
        <input type="text" name="employeeid" placeholder="Employee ID/Student ID" required>
        <x-input-error :messages="$errors->get('employeeid')"/>
        <input type="email" name="email" placeholder="Email Address" required>
        <x-input-error :messages="$errors->get('email')"/>
        <input type="number" name="contact" placeholder="Contact Number">
        <x-input-error :messages="$errors->get('contact')"/>
        <select type="text" name="role" type="text" required>
            <option value="">Register as</option>
            <option value="student">Student</option>
            <option value="staff">Staff</option>
        </select>
        <x-input-error :messages="$errors->get('role')"/>
        <input type="password" name="password" placeholder="Password" required>
        <x-input-error :messages="$errors->get('password')"/>
        <input type="password" name="password_confirmation" placeholder="Confirm Password" required>
        <x-input-error :messages="$errors->get('password_confirmation')"/>
        <input type="submit" value="Register">
    </form>
    </div>
    <script src="../path/to/flowbite/dist/flowbite.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
    <script>
    document.getElementById('role').addEventListener('change', validateEmployeeID);
    document.getElementById('employeeid').addEventListener('input', validateEmployeeID);

    function validateEmployeeID() {
        const role = document.getElementById('role').value;
        const employeeid = document.getElementById('employeeid').value;
        const employeeidError = document.getElementById('employeeidError');

        if (employeeid.trim() === '') {
            employeeidError.textContent = ''; // Clear error message if input is empty
            return true; // Skip validation if input is empty
        }

        if (role === 'student') {
            if (!/^\d{8}$/.test(employeeid) || !employeeid.startsWith('1')) {
                employeeidError.textContent = 'The Student ID you have provided is incorrect!';
                return false;
            }
        } else if (role === 'staff') {
            if (!/^RUB\d{8}$/.test(employeeid)) {
                employeeidError.textContent = 'The Staff ID you have provided is incorrect!';
                return false;
            }
        }

        employeeidError.textContent = '';
        return true;
    }

    document.getElementById('registrationForm').addEventListener('submit', function (event) {
        if (!validateEmployeeID()) {
            event.preventDefault();
        }
    });
</script>

</body>
</html> -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <title>gc</title>
</head>
<body>
<section class="h-100 gradient-form" style="background-color: #eee;">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col-xl-10">
              <div class="card rounded-3 text-black">
                <div class="row g-0">
                <div class="col-lg-6 d-flex align-items-center justify-content-center wrapper">
                    <img src="img/bg.jpeg" alt="Background Image">
                    <div class="text-white px-3 py-4 p-md-5 mx-md-4 text-container d-flex flex-column justify-content-center align-items-center text-center">
                        <h4 class="mb-4">Welcome Back</h4>
                        <p class="small mb-0">Gain access to our facility booking system to easily <br> reserve spaces and amenities for your events <br> and activities. Log in now to manage your<br> bookings and streamline your <br>planning process.</p>
                        <br> <br>
                        <p class="text3">Already have an account?</p>
                        <a class="btn btn2 btn-outline-light mt-3" type="button" href="{{route('login')}}">Login Now</a>
                        
                    </div>
                </div>


                  <div class="col-lg-6 d-flex align-items-center justify-content-center">
                    <div class="card-body align-items-center justify-content-center p-md-5 mx-md-4">     
                       
                        <img class="rlogo" src="{{asset('img/logo.png')}}" />
    <form action="{{route('register')}}" method="POST">
        @csrf
        <div class="form-outline mb-4">
        <input type="text" name="name" placeholder="Name" required>
        <x-input-error :messages="$errors->get('name')"/>
</div>
<div class="form-outline mb-4">
        <input type="text" name="employeeid" placeholder="Employee ID/Student ID" required>
        <x-input-error :messages="$errors->get('employeeid')"/>
</div>
<div class="form-outline mb-4">
        <input type="email" name="email" placeholder="Email Address" required>
        <x-input-error :messages="$errors->get('email')"/>
</div>
<div class="form-outline mb-4">
        <input type="number" name="contact" placeholder="Contact Number">
        <x-input-error :messages="$errors->get('contact')"/>
</div>
<div class="form-outline mb-4">
        <select type="text" name="role" type="text" required>
            <option value="">Register as</option>
            <option value="student">Student</option>
            <option value="staff">Staff</option>
        </select>
        <x-input-error :messages="$errors->get('role')"/>
</div>
<div class="form-outline mb-4">
        <input type="password" name="password" placeholder="Password" required>
        <x-input-error :messages="$errors->get('password')"/>
</div>
<div class="form-outline mb-4">
        <input type="password" name="password_confirmation" placeholder="Confirm Password" required>
        <x-input-error :messages="$errors->get('password_confirmation')"/>
</div>
       
        <button class="btn btn1 btn-primary btn-block fa-lg mb-3" type="submit" value="Register">Register</button>
    </form>
                      @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">
                            {{ __('Forgot password?') }}
                        </a>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    
       

    <script>
    document.getElementById('role').addEventListener('change', validateEmployeeID);
    document.getElementById('employeeid').addEventListener('input', validateEmployeeID);

    function validateEmployeeID() {
        const role = document.getElementById('role').value;
        const employeeid = document.getElementById('employeeid').value;
        const employeeidError = document.getElementById('employeeidError');

        if (employeeid.trim() === '') {
            employeeidError.textContent = ''; // Clear error message if input is empty
            return true; // Skip validation if input is empty
        }

        if (role === 'student') {
            if (!/^\d{8}$/.test(employeeid) || !employeeid.startsWith('1')) {
                employeeidError.textContent = 'The Student ID you have provided is incorrect!';
                return false;
            }
        } else if (role === 'staff') {
            if (!/^RUB\d{8}$/.test(employeeid)) {
                employeeidError.textContent = 'The Staff ID you have provided is incorrect!';
                return false;
            }
        }

        employeeidError.textContent = '';
        return true;
    }

    document.getElementById('registrationForm').addEventListener('submit', function (event) {
        if (!validateEmployeeID()) {
            event.preventDefault();
        }
    });
</script>

    <script src="../path/to/flowbite/dist/flowbite.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>

