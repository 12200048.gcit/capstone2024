<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>gc</title>
</head>
<body>
<section class="h-100 gradient-form" style="background-color: white;">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col-xl-10">
              <div class="card rounded-3 text-black">
                <div class="row g-0">
                <div class="col-lg-6 d-flex align-items-center justify-content-center wrapper">
                    <img src="img/bg.jpeg" alt="Background Image">
                    <div class="text-white px-3 py-4 p-md-5 mx-md-4 text-container d-flex flex-column justify-content-center align-items-center text-center">
                        <h4 class="mb-4">Welcome Back</h4>
                        <p class="small mb-0">Gain access to our facility booking system to easily <br> reserve spaces and amenities for your events <br> and activities. Log in now to manage your<br> bookings and streamline your <br>planning process.</p>
                        <br> <br>
                        <p class="text3">Don't have an account?</p>
                        <a class="btn btn2 btn-outline-light mt-3" type="button" href="{{route('register')}}">Register Now</a>
                    </div>
                </div>


                  <div class="col-lg-6 d-flex align-items-center justify-content-center">
                    <div class="card-body align-items-center justify-content-center p-md-5 mx-md-4">     
                        <img src="{{asset('img/logo.png')}}" style="width: 150px; height: 150px; margin-left: 30%;" alt="logo">    
                      <form action="{{route('login.post')}}" method="POST">
                        @csrf
                        <x-input-error :messages="$errors->get('email')"  />
                        <x-input-error :messages="$errors->get('password')"  />

                        <div class="form-outline mb-4">
                          <input type="email" name="email" class="form-control" placeholder="Email" required/>
                        </div>
      
                        <div class="form-outline mb-4">
                          <input type="password" name="password" class="form-control" placeholder="Password" required/>
                        </div>
      
                        <div class="text-center pt-1 mb-5 pb-1">
                          <button class="btn btn-primary btn-block fa-lg mb-3 btn1" type="submit" value="Login">Log
                            in</button></br>
                            @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">
                            {{ __('Forgot password?') }}
                        </a>

                      @endif
                        </div>
                      </form>
                    
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    
        <!-- <div class="card1login col-4">
            <img class="bg" src="img/bg.jpeg" alt="Background Image">
            <div >
                <p class="text">Welcome Back</p>
                <p class="text1">Gain access to our facility booking system to easily <br> reserve spaces and amenities for your events <br> and activities. Log in now to manage your<br> bookings and streamline your <br>planning process.</p>
                <p class="text3">Don't have an account?</p>
                <a class="button" href="{{route('register')}}">Register Now</a>
            </div>
        </div>
        <div class="cardlogin col-8">
            <img class="rlogo" src="{{asset('img/logo.png')}}" /> 

            <form action="{{route('login.post')}}" method="POST">
            @csrf
            <x-input-error :messages="$errors->get('email')"  />
            <x-input-error :messages="$errors->get('password')"  />
            <input type="email" name="email" placeholder="Email" required>
            <input type="password" name="password" placeholder="Password" required>
            <br>
            <input type="submit" value="Login">
            </form>
                @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}">
                    {{ __('Forgot password?') }}
                </a>
                @endif
        </div>
    </div> -->


    <script src="../path/to/flowbite/dist/flowbite.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>