<x-user-layout>
  <style>
    .card1-header {
        display: flex;
        align-items: center;
        justify-content: space-between;
        gap: 10px; /* Adjust the gap as needed */
    }
    .card1-header h2 {
        margin: 0; /* Remove default margin */
    }
    .card1-header form {
        margin: 0; /* Remove default margin */
    }
</style>
  <div class="card1" style="margin: 4% auto; width: 90%; max-width: 900px;">
    <div class="card1-header">
        <h2>My Bookings</h2>
        <form id="userTypeForm" action="{{route('mybookings')}}" method="GET">
          @csrf
          <select style="border-radius: 5px" name="mybooking" onchange="this.form.submit()">
              <option>Select Status</option>
              <option value="All">All</option>
              <option value="Pending">Pending</option>
              <option value="Accepted">Accepted</option>
              <option value="Denied">Denied</option>
          </select>
      </form>
    </div>
    <div class="card1-body">
        <table>
            <thead>
                <tr>
                    <th>Sl No</th>
                    <th>Image</th>
                    <th>Facility</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              @php
                $serialNumber = 1; // Initialize the serial number counter
              @endphp
              @foreach ($bookings as $booking)
                <tr>
                    <td>{{ $serialNumber }}</td>
                    <td>
                      <img src="{{ asset('storage/' . $booking->facility->image) }}" width="60px" height="60px"/>
                    </td>
                    <td>{{ $booking->facility->facility_name }}</td>
                    <td>{{$booking->starttime}}</td>
                    <td>{{$booking->endtime}}</td>
                     @if($booking->status === 'Pending')
                            <td style="font-size:12px;padding-Left:5px;color:blue">{{$booking->status}}</td>
                        @elseif($booking->status === 'Accepted')
                            <td style="font-size:12px;padding-Left:5px;color:green">{{$booking->status}}</td>
                        @else
                            <td style="font-size:12px;padding-Left:5px;color:red">{{$booking->status}}</td>
                        @endif
                 
                    <td>
                      <form action="{{ route('user.deleteBooking', $booking->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button style="color:rgb(218, 64, 64)" type="submit" onclick="return confirm('Are you sure you want to delete this item?')">Delete</button>
                    </form>
                    </td>
                </tr>
                @php
                  $serialNumber++; // Increment the serial number for the next iteration
                @endphp
                @endforeach
                
            </tbody>
        </table>
        <p style="text-align:center">
          << @if ($bookings->lastPage() > 1)
              @for ($i = 1; $i <= $bookings->lastPage(); $i++)
                  <a style="margin-inline: 10px" href="{{ $bookings->url($i) }}">{{ $i }}</a>
              @endfor
          @endif >>
      </p>
    </div>
  
</div>

<script src="../path/to/flowbite/dist/flowbite.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>

</body>


</x-user-layout>
