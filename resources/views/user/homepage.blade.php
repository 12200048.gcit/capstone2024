<x-user-layout>

  <div class="container" >
<!-- HTML code -->
<div class="d-flex w-100 px-5 mb-4">
  <div  >
    <p class= "name">Welcome back, <span style="color: #0D4C85; text-transform: uppercase;" >{{ Auth::user()->name }}!</span></p>
  {{-- <div class="ms-auto">
    <input style="height: 30px" id="searchInput" class="me-2 rounded" type="search" placeholder="Search by name" aria-label="Search">
  
    
      <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="blue" fill-rule="evenodd" d="M11 2a9 9 0 1 0 5.618 16.032l3.675 3.675a1 1 0 0 0 1.414-1.414l-3.675-3.675A9 9 0 0 0 11 2m-6 9a6 6 0 1 1 12 0a6 6 0 0 1-12 0" clip-rule="evenodd"/></svg>
   
  </div> --}}
  <div style="position: relative;">
    <input style="height: 30px; padding-left: 30px;" id="searchInput" class="me-2 rounded" type="search" placeholder="Search by name" aria-label="Search">
    <svg style="position: absolute; top: 50%; left: 5px; transform: translateY(-50%);" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="#0D4C85" fill-rule="evenodd" d="M11 2a9 9 0 1 0 5.618 16.032l3.675 3.675a1 1 0 0 0 1.414-1.414l-3.675-3.675A9 9 0 0 0 11 2m-6 9a6 6 0 1 1 12 0a6 6 0 0 1-12 0" clip-rule="evenodd"/></svg>
</div>

  </div>
  
</div>

@foreach ($facilities as $facility)
  <div style="background-color: #F5F5F5; color: #000; shadow: 2px 2px 4px rgba(0, 0, 0, 0.5); " class="card facility-card">
    <img src="{{ Storage::url($facility->image) }}" alt="Image" class="card-image">
    <div class="card-content">
      <h3 class="card-title">{{$facility->facility_name}}</h3>
      <p class="card-text">{{$facility->desc}}</p>
      <a href="{{ route('user.bookingpage', ['id' => $facility->id]) }}" class="btnn">BOOK NOW</a>      
    </div>
  </div>
@endforeach
<script>
  // JavaScript code
document.addEventListener("DOMContentLoaded", function() {
  const facilities = {!! json_encode($facilities) !!}; // Assuming $facilities is a PHP array
  
  // Function to render facilities based on search input
  function renderFacilities(searchTerm) {
    const facilitiesList = document.querySelectorAll('.facility-card');
    
    facilitiesList.forEach(facility => {
      const facilityName = facility.querySelector('.card-title').textContent.toLowerCase();
      if (facilityName.includes(searchTerm.toLowerCase())) {
        facility.style.display = 'block';
      } else {
        facility.style.display = 'none';
      }
    });
  }
  
  // Initial render
  renderFacilities('');
  
  // Add event listener for input field
  document.getElementById('searchInput').addEventListener('input', function(event) {
    renderFacilities(event.target.value);
  });

  // Add event listener for search button
  document.getElementById('searchButton').addEventListener('click', function() {
    const searchTerm = document.getElementById('searchInput').value;
    renderFacilities(searchTerm);
  });
});

</script>
  </div>


</x-user-layout>

