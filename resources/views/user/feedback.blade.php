
<x-user-layout>
<div class="d-flex">
  <div class="card2 ms-auto me-auto   ">
    <div class="card2-header">
        <h2 class="ftext">Help Us Improve</h2>
        <p class="pp">Share your  thoughts on any changes that needs to be made in our<br> application. We promise to meet your expectation!</p>
        <form action="{{ route('feedback.store') }}" method="POST">
           @csrf
            <textarea name="content" rows="7" cols="50" placeholder="Enter your feedback here..."></textarea>
             <br>
             <div class="d-flex">
            <button class="btn btn-outline-primary btn-sm rounded-1 mt-3 ms-auto me-auto px-3" type="submit">Submit </button>
            </div>
      </form>

        <!-- btnnnn<textarea class="dotted-border" placeholder="write your feedbacks"></textarea>

        <a href="#" class="btnnnn">Submit</a> -->
    </div>
    </div>
</div>


    <script src="../path/to/flowbite/dist/flowbite.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
    @if (Session::has('success'))
      <script>
        swal("Success!","{{Session::get('success')}}",'success',{
          button:true,
          button:"OK"
        });
      </script>
    @endif
</body>
{{-- </html> --}}

</x-user-layout>
