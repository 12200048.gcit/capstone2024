<x-app-layout>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <section style="padding-block: 30px;display:flex;justify-content:space-evenly">
        
        <div class="card border-light mb-3" style="width: 20rem;height:30rem;">
          <div class="card-header" style="font-weight:600;font-size:18px; ">Update Profile </div>

            <form action="{{route('updateProfile',['id'=>Auth::user()->id])}}"  style="padding: 1rem; display:flex; flex-direction:column; align-item:center; justify-contet:center" method="POST">
                @csrf
                <span style="font-weight:600">Name</span>
                <input value={{Auth::user()->name}} type="text" id="name" name="name" placeholder="Enter name" ><br>
                <span style="font-weight:600">Email</span>
                <input value={{Auth::user()->email}} type="email" id="name" name="email" placeholder="Enter email"><br>
                <span style="font-weight:600">Employeeid</span>
                <input value={{Auth::user()->employeeid}} type="text" id="name" name="employeeid" placeholder="Enter employeeid"><br>
                <span style="font-weight:600">contact</span>
                <input value={{Auth::user()->contact}} type="text" id="name" name="contact" placeholder="Enter contact"><br>
                <button type="submit" class="btn btn-primary" style="width:100%;margin-bottom:20px" >Save</button>
            </form>

          </div>
        </div>
        
      </section>

      @if(Session::has('success'))
      <script>
          swal("Message", "{{ Session::get('success') }}", 'success', {
              button: "OK",
              timer: 3000,
          })
      </script>
      @endif
        @if(Session::has('error'))
            <script>
                swal("Message", "{{ Session::get('error') }}", 'error', {
                    button: "OK",
                    timer: 3000,
                })
            </script>
      @endif
</x-app-layout>