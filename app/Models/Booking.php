<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Facility;
use App\Models\User;

class Booking extends Model{
    use HasFactory;
    protected $fillable = [
        'facility_id',
        'user_id',
        'date',
        'starttime',
        'endtime',
        'reason',
        'status',
        'reason'
    ];
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function facility()
    {
        return $this->belongsTo(Facility::class, 'facility_id');
    }
}
