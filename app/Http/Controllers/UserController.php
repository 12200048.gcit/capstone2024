<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\User;

use App\Models\Facility;
use App\Models\Feedback;
use Illuminate\Http\Request;
// use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
   

    public function homepage()
    {
        $facilities = Facility::where('status', 'Available')->get();
        $userRole = auth()->user()->role; // Assuming you are using Laravel's authentication

        // Filter facilities based on user role
        if ($userRole == 'student') {
            // Filter out facilities where role is 'staff' (students cannot access staff-only facilities)
            $facilities = $facilities->reject(function ($facility) {
                return $facility->role == 'staff';
            });
        }

        return view('user.homepage', compact('facilities'));
    }
    public function Bookingpage($id)
    {
        $bookings = Booking::where('facility_id', $id)
                       ->select('date', 'starttime') // Select only 'date' and 'starttime' columns
                       ->get();

    // Extract 'date' and 'starttime' from each booking into a new array
    $bookingsData = $bookings->map(function ($booking) {
        return [
            'date' => $booking->date,
            'starttime' => $booking->starttime,
        ];
    })->toArray();
        $facility = Facility::where('id', $id)->first();
        return view('user.bookingpage', compact('facility', 'bookingsData'));
    }
    
    public function bookingPageCreate(Request $request)
{
    $request->validate([
        'facility_id' => ['required', 'exists:facilities,id'],
        'date' => ['required', 'date', 'after_or_equal:today'], // Ensures date is today or in the future
        'time' => ['required'],
        'reason' => ['required', 'string'],
    ]);

    $user = Auth::user(); // Get the authenticated user
    $facilityId = $request->input('facility_id');

    // Retrieve the facility information
    $facility = Facility::findOrFail($facilityId);

    // Get the start time from the request (in 'H:i A' format)
    $startTimeA = $request->input('time');

    // Parse the start time from 'H:i A' format to 'H:i' format using Carbon
    $startTimeObj = Carbon::createFromFormat('h:i A', $startTimeA);

    // Format the start time as 'H:i'
    $startTime = $startTimeObj->format('H:i');

    // Get the step duration (in H:i:s format) from the facility and convert to seconds
    $step = $facility->step;
    list($stepHours, $stepMinutes, $stepSeconds) = explode(':', $step);
    $stepInSeconds = $stepHours * 3600 + $stepMinutes * 60 + $stepSeconds;

    // Calculate the end time by adding the step duration (in seconds) to the start time
    $endTimeObj = $startTimeObj->copy()->addSeconds($stepInSeconds);

    // Format the end time as 'H:i'
    $endTime = $endTimeObj->format('H:i');

    // Create a new booking record
    Booking::create([
        'facility_id' => $facilityId,
        'user_id' => $user->id,
        'date' => $request->input('date'),
        'starttime' => $startTime,
        'endtime' => $endTime,
        'reason' => $request->input('reason'),
    ]);

    return redirect()->back()->with('success', 'Successfully Booked');
}
    // Helper method to calculate end time based on start time and step duration
    // private function calculateEndTime($startTime, $step)
    // {
        


    //     return $endTimeObj->format('H:i'); // Format end time as 'HH:mm'
    // }
    public function Feedback()
    {
        return view('user.feedback');
    }

    public function Mybooking()
    {
        $userId = Auth::id(); // Get the ID of the currently authenticated user

        // Fetch bookings associated with the current user and where status is 'Pending'
        $bookings = Booking::where('user_id', $userId)
                    ->with('facility')
                    ->paginate(3);        

        return view('user.mybooking',compact('bookings'));
    }

    

    function DeleteBooking($id){
        $booking = Booking::findOrFail($id); // Find the item by its ID

        $booking->delete(); // Delete the item

        return redirect()->back()->with('success', 'Item deleted successfully');
    }

    public function Profile()
    {
        return view('user.profile');
    }

    public function update(Request $request, User $user){
        $userData = $request->all();

        // Update user record with the input data
        $user->update($userData);

        return redirect()->back()->with('success', 'User details updated successfully');
    }

    public function destroy(User $user){
        $user->delete(); // Delete the user
        return redirect()->route('login')->with('success', 'User deleted successfully');
    }
    public function FeedbackPost(Request $request){
        $request->validate([
            'content' => 'required',
        ]);

        // Get the start time from the request (in 'H:i A' format)
        $content = $request->input('content');

        // Create a new booking record
        Feedback::create([
            'content' => $content
        ]);
        return redirect(route('user.feedback'))->with('success','Feedback Submitted Successfully!');
    }

    public function mybooking2(Request $request) {
        $userType = $request->input('mybooking');
        $userId = Auth::id();

        if ($userType == 'All') {
            // Fetch all bookings associated with the current user
            $bookings = Booking::where('user_id', $userId)
                            ->with('facility')
                            ->paginate(3); 
        }
        elseif($userType == 'Pending'){
            // Fetch bookings associated with the current user and where status is 'Pending'
            $bookings = Booking::where('user_id', $userId)
                            ->where('status', 'Pending')
                            ->with('facility')
                            ->paginate(3); ;
        }
        elseif($userType == 'Denied'){
            // Fetch bookings associated with the current user and where status is 'Pending'
            $bookings = Booking::where('user_id', $userId)
                            ->where('status', 'Denied')
                            ->with('facility')
                            ->paginate(3); 
        }
        else{
            // Fetch bookings associated with the current user and where status is 'Pending'
            $bookings = Booking::where('user_id', $userId)
                            ->where('status', 'Accepted')
                            ->with('facility')
                            ->paginate(3); 
        }

        return view('user.mybooking', compact('bookings'));
    }
}
