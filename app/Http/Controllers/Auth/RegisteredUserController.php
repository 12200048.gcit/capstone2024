<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;
use App\Rules\CustomEmailFormat;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    public function LinkRegister()
    {
        return view('auth.link-register'); // 'page' is the name of your blade file
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:'.User::class],
            'employeeid' => ['required', 'unique:'.User::class],
            'contact'=>['required', 'regex:/^(17|77)\d{6}$/', 'size:8','unique:'.User::class],
            'role' => ['required'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);
        

$request->validate([
    'name' => ['required', 'string', 'max:255'],
    'email' => [
        'required',
        'string',
        'lowercase',
        'max:255',
        function ($attribute, $value, $fail) use ($request) {
            if ($request->role !== 'admin') {
                if (!preg_match('/^\d{8}\.gcit@rub\.edu\.bt$/', $value)) {
                    $fail('The email format should be (Employee ID/Student ID).sherubtse@rub.edu.bt');
                }
            }
        },
        'unique:'.User::class,
    ],
    'employeeid' => ['required'],
    'contact' => ['required', 'regex:/^(17|77)\d{6}$/', 'size:8', 'unique:'.User::class],
    'role' => ['required'],
    'password' => ['required', 'confirmed', Rules\Password::defaults()],
]);


        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'employeeid' => $request->employeeid,
            'contact' => $request->contact,
            'role' => $request->role,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        // Auth::login($user);

        return redirect(route('login', absolute: false));
        

    }
}
